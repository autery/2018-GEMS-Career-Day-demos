pen = canvas.getContext('2d')
points = []

onclick = function addPoint(point) {
    points.push(point)
}

onmousemove = function draw(cursor) {
    pen.clearRect(0, 0, 1440, 900)
    pen.beginPath()
    points.forEach(point => {
        pen.rect(point.x - 5, point.y - 5, 10, 10)
        pen.moveTo(point.x, point.y)
        pen.lineTo(cursor.x, cursor.y)
    })
    pen.stroke()
}
